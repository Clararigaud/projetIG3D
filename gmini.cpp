// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include "src/GLProgram.h"
#include "src/Exception.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cstdio>
#include <cstdlib>

#include <algorithm>
#include "src/Vec3.h"

#include <GL/glut.h>
#include "src/matrixUtilities.h"
#include "src/imageLoader.h"
#include "src/Scene.h"
#include "src/Sphere.h"
#include "src/Mesh.h"
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX=0, lastY=0, lastZoom=0;
static unsigned int FPS = 0;
static bool fullScreen = false;
static unsigned int SCREENWIDTH = 640;
static unsigned int SCREENHEIGHT = 480;
GLint window;
Scene scene;

void printUsage () {
    cerr << endl
         << "gMini: a minimal OpenGL/GLUT application" << endl
         << "for 3D graphics." << endl
         << "Author : Tamy Boubekeur (https://perso.telecom-paristech.fr/boubek/)" << endl << endl
         << "Modified by : Jean-Marc Thiery (https://perso.telecom-paristech.fr/jthiery/)" << endl << endl
         << "Usage : ./gmini [<file.off>]" << endl
         << "Keyboard commands" << endl
         << "------------------" << endl
         << " ?: Print help" << endl
         << " w: Toggle Wireframe Mode" << endl
         << " g: Toggle Gouraud Shading Mode" << endl
         << " f: Toggle full screen mode" << endl
         << " <drag>+<left button>: rotate model" << endl
         << " <drag>+<right button>: move model" << endl
         << " <drag>+<middle button>: zoom" << endl
         << " q, <esc>: Quit" << endl << endl;
}

void usage () {
    printUsage ();
    exit (EXIT_FAILURE);
}
void display () {
    glLoadIdentity ();
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    scene.camera.apply();
    scene.draw();
    glFlush ();
    glutSwapBuffers ();
}


void idle () {
    static float lastTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
    static unsigned int counter = 0;
    counter++;
    float currentTime = glutGet ((GLenum)GLUT_ELAPSED_TIME);
    if (currentTime - lastTime >= 1000.0f) {
	FPS = counter;
	counter = 0;
	static char winTitle [64];
	sprintf (winTitle, "gmini - FPS: %d", FPS);
	glutSetWindowTitle (winTitle);
	lastTime = currentTime;
    }
    glutPostRedisplay ();
    scene.time = currentTime;
}

void key (unsigned char keyPressed, int x, int y) {    
    switch (keyPressed) {
    case 'f':
	if (fullScreen == true) {
	    glutReshapeWindow (SCREENWIDTH, SCREENHEIGHT);
	    fullScreen = false;
	} else {
	    glutFullScreen ();
	    fullScreen = true;
	}
	break;
    case 'q':	
    case 27:
	exit (0);
	break;
    case 'w':
	glPolygonMode (GL_FRONT_AND_BACK, GL_LINE);
	break;
    case 'g':
	glPolygonMode (GL_FRONT_AND_BACK, GL_FILL);
	break;
    case 'r':
	scene.rayTraceFromCamera();
    default:
	printUsage ();
	break;
    }
    idle ();
}

void mouse (int button, int state, int x, int y) {
    if (state == GLUT_UP) {
	mouseMovePressed = false;
	mouseRotatePressed = false;
	mouseZoomPressed = false;
    } else {
	if (button == GLUT_LEFT_BUTTON) {
	    scene.camera.beginRotate (x, y);
	    mouseMovePressed = false;
	    mouseRotatePressed = true;
	    mouseZoomPressed = false;
	} else if (button == GLUT_RIGHT_BUTTON) {
	    lastX = x;
	    lastY = y;
	    mouseMovePressed = true;
	    mouseRotatePressed = false;
	    mouseZoomPressed = false;
	} else if (button == GLUT_MIDDLE_BUTTON) {
	    if (mouseZoomPressed == false) {
		lastZoom = y;
		mouseMovePressed = false;
		mouseRotatePressed = false;
		mouseZoomPressed = true;
	    }
	}
    }
    idle ();
}

void motion (int x, int y) {
    if (mouseRotatePressed == true) {
	scene.camera.rotate (x, y);
    }
    else if (mouseMovePressed == true) {
	scene.camera.move ((x-lastX)/static_cast<float>(SCREENWIDTH), (lastY-y)/static_cast<float>(SCREENHEIGHT), 0.0);
	lastX = x;
	lastY = y;
    }
    else if (mouseZoomPressed == true) {
	scene.camera.zoom (float (y-lastZoom)/SCREENHEIGHT);
	lastZoom = y;
    }
}


void reshape(int w, int h) {
    scene.camera.resize (w, h);
}



void bigDemo(){
    Material redDiffuse;
    redDiffuse.diffuseColor = Vec3(1,0,0);
    redDiffuse.ambiant= Vec3(1,0,0);
    redDiffuse.specular = Vec3(0,0,0);
    redDiffuse.ka = 0.7;
    redDiffuse.kd = 0.75;
    redDiffuse.ks = 0.0;
    redDiffuse.shininess = 0.01;
    redDiffuse.mirror = false;

    Material greenDiffuse;
    greenDiffuse.diffuseColor = Vec3(0,1,0);   
    greenDiffuse.ambiant= Vec3(0,1,0);
    greenDiffuse.specular = Vec3(0,0,0);
    greenDiffuse.ka = 0.7;
    greenDiffuse.kd = 0.7;
    greenDiffuse.ks = 0.0;
    greenDiffuse.shininess = 0.01;
    greenDiffuse.mirror = false;

    Material blueDiffuse;
    blueDiffuse.diffuseColor = Vec3(0,0,1);   
    blueDiffuse.ambiant= Vec3(0,0,1);
    blueDiffuse.specular = Vec3(0,0,0);
    blueDiffuse.ka = 1.0;
    blueDiffuse.kd = 1.0;
    blueDiffuse.ks = 0.7;
    blueDiffuse.shininess = 10.1;
    blueDiffuse.mirror = false;

    Material mirror;
    mirror.diffuseColor = Vec3(1,1,1);
    mirror.ambiant= Vec3(0,0,0);
    mirror.specular = Vec3(0,0,0);
    mirror.ka = 1.0;
    mirror.kd = 1.0;
    mirror.ks = 1.0;
    mirror.shininess = 0.01;
    mirror.mirror = true;
    mirror.kr = 1.0;

   Material transparent;
    transparent.diffuseColor = Vec3(1,1,1);
    transparent.ambiant= Vec3(0,0,0);
    transparent.specular = Vec3(0,0,0);
    transparent.ka = 1.0;
    transparent.kd = 1.0;
    transparent.ks = 1.0;
    transparent.shininess = 0.01;
    transparent.transparency.on = true;
    transparent.transparency.IOR = 1.3;
    transparent.kr = 0.0;

    Mesh BoxLeft = Mesh("models/BoxLeft.off");
    BoxLeft.material = redDiffuse;
    scene.addMesh (BoxLeft);

    Mesh BoxRight = Mesh("models/BoxRight.off");
    BoxRight.material = greenDiffuse;
    scene.addMesh (BoxRight);

    Mesh BoxBack = Mesh("models/BoxBack.off");
    BoxBack.material = blueDiffuse;
    scene.addMesh (BoxBack);

    Mesh BoxBottom = Mesh("models/BoxBottom.off");
    BoxBottom.material.ambiant = Vec3(0.6,1.0,0.6);
    BoxBottom.material.diffuseColor = Vec3(1.0,1.0,1.0);
    BoxBottom.material.specular = Vec3(1.0,1.0,1.0);
    BoxBottom.material.ks = 0.0001;
    scene.addMesh (BoxBottom);

    // Mesh BoxTop = Mesh("models/BoxTop.off");
    // BoxTop.material.ambiant = Vec3(0.7,0.7,0.7);
    // BoxTop.material.diffuseColor = Vec3(0.7,0.7,0.7);
    // BoxTop.material.specular = Vec3(0.7,1.0,0.3);
    // BoxTop.material.ks = 0.0001;
    // scene.addMesh (BoxTop);

    float sSize = 0.3f;
    Sphere s2 = Sphere(Vec3(-0.4,-2.0+sSize/2.0f,-0.4), sSize);
    s2.material = blueDiffuse;

    Sphere s5 = Sphere(Vec3(0.4,-2.0+sSize/2.0f,-0.4), sSize);
    s5.material = redDiffuse;

    Sphere s1 = Sphere(Vec3(-0.8,-2.0+sSize/2.0f,0.4), sSize);
    s1.material = mirror;

    Sphere s4 = Sphere(Vec3(0.8,-2.0+sSize/2.0f,0.4), sSize);
    s4.material = transparent;

    // scene.addSphere(s1);
    // scene.addSphere(s2);
    // scene.addSphere(s5);
    scene.addSphere(s4);
}

void objMeshDemo(){
    Mesh cube = Mesh("models/cubeTex.obj"); // call to Objloader
    cube.material.diffuseColor = Vec3(0.7,0.8,1.0);
    cube.material.ambiant= Vec3(0.7,1,1);
    cube.material.specular = Vec3(0,0,0);
    cube.material.ka = 1.0;
    cube.material.kd = 1.0;
    cube.material.ks = 0.3;
    cube.material.shininess = 0.01;
    cube.material.mirror = false;
    scene.addMesh(cube);
}

void textureMappingDemo(){
    Mesh plane = Mesh("models/sphereTex.obj"); // call to Objloader
    plane.material.hasTexture = true;
    plane.material.diffuseColor = Vec3(1.0,1.0,1.0);
    plane.material.ambiant = Vec3(1.0,1.0,1.0);
    plane.material.ka = 1.0;
    plane.material.kd = 1.0;
    plane.material.ks = 0.3;
    plane.material.texture.colorMapImageUrl = "img/sphereTextures/s1.ppm";

    scene.addMesh(plane);
}

void refractionDemo(){
        Material redDiffuse;
    redDiffuse.diffuseColor = Vec3(1,0,0);
    redDiffuse.ambiant= Vec3(1,0,0);
    redDiffuse.specular = Vec3(0,0,0);
    redDiffuse.ka = 0.7;
    redDiffuse.kd = 0.75;
    redDiffuse.ks = 0.0;
    redDiffuse.shininess = 0.01;
    redDiffuse.mirror = false;

    Material greenDiffuse;
    greenDiffuse.diffuseColor = Vec3(0,1,0);   
    greenDiffuse.ambiant= Vec3(0,1,0);
    greenDiffuse.specular = Vec3(0,0,0);
    greenDiffuse.ka = 0.7;
    greenDiffuse.kd = 0.7;
    greenDiffuse.ks = 0.0;
    greenDiffuse.shininess = 0.01;
    greenDiffuse.mirror = false;

    Material blueDiffuse;
    blueDiffuse.diffuseColor = Vec3(0,0,1);   
    blueDiffuse.ambiant= Vec3(0,0,1);
    blueDiffuse.specular = Vec3(0,0,0);
    blueDiffuse.ka = 1.0;
    blueDiffuse.kd = 1.0;
    blueDiffuse.ks = 0.7;
    blueDiffuse.shininess = 10.1;
    blueDiffuse.mirror = false;

    Material mirror;
    mirror.diffuseColor = Vec3(1,1,1);
    mirror.ambiant= Vec3(0,0,0);
    mirror.specular = Vec3(0,0,0);
    mirror.ka = 1.0;
    mirror.kd = 1.0;
    mirror.ks = 1.0;
    mirror.shininess = 0.01;
    mirror.mirror = true;
    mirror.kr = 1.0;

   Material transparent;
    transparent.diffuseColor = Vec3(1,1,1);
    transparent.ambiant= Vec3(0,0,0);
    transparent.specular = Vec3(0,0,0);
    transparent.ka = 1.0;
    transparent.kd = 1.0;
    transparent.ks = 1.0;
    transparent.shininess = 0.01;
    transparent.transparency.on = true;
    transparent.transparency.IOR = 1.3;
    transparent.kr = 0.0;
    Mesh BoxLeft = Mesh("models/BoxLeft.off");
    BoxLeft.material = redDiffuse;
    scene.addMesh (BoxLeft);

    Mesh BoxRight = Mesh("models/BoxRight.off");
    BoxRight.material = greenDiffuse;
    scene.addMesh (BoxRight);

    Mesh BoxBack = Mesh("models/BoxBack.off");
    BoxBack.material = blueDiffuse;
    scene.addMesh (BoxBack);

    Mesh BoxBottom = Mesh("models/BoxBottom.off");
    BoxBottom.material.ambiant = Vec3(0.6,1.0,0.6);
    BoxBottom.material.diffuseColor = Vec3(1.0,1.0,1.0);
    BoxBottom.material.specular = Vec3(1.0,1.0,1.0);
    BoxBottom.material.ks = 0.0001;
    scene.addMesh (BoxBottom);


    Mesh suzan = Mesh("models/monkey.off");
    suzan.material = transparent;
    scene.addMesh(suzan);
}

void shadeRefractDemo(){
    Material redDiffuse;
    redDiffuse.diffuseColor = Vec3(1,0,0);
    redDiffuse.ambiant= Vec3(1,0,0);
    redDiffuse.specular = Vec3(0,0,0);
    redDiffuse.ka = 0.7;
    redDiffuse.kd = 0.0;
    redDiffuse.ks = 0.0;
    redDiffuse.shininess = 0.01;
    redDiffuse.mirror = false;

    Sphere s5 = Sphere(Vec3(0.4,0.0,-0.6), 0.5);
    s5.material = redDiffuse;
    scene.addSphere(s5);

    Sphere s1 = Sphere(Vec3(-0.4,0.0,0.0), 0.5);
    s1.material = redDiffuse;
    s1.material.diffuseColor = Vec3(1.0,1.0,1.0);
    s1.material.ambiant = Vec3(0.7,0.7,0.0);
    s1.material.specular = Vec3(1.0,1.0,1.0);
    s1.material.ks = 1.0;
    s1.material.shininess = 2.0;
    scene.addSphere(s1);

    Sphere s2 = Sphere(Vec3(0.3,0.3,0.5), 0.2);
    s2.material = redDiffuse;
    s2.material.diffuseColor = Vec3(1.0,1.0,1.0);
    s2.material.ambiant = Vec3(1.0,1.0,1.0);
    s2.material.specular = Vec3(1.0,1.0,1.0);
    s2.material.kd = 0.3;
    s2.material.ks = 1.0;
    s2.material.shininess = 50.0;
    s2.material.transparency.on = true;
    s2.material.transparency.IOR = 1.3;
    s2.material.kr = 0.5;
    scene.addSphere(s2);
}

void showGizmo(){
    scene.addSphere( Vec3(0,0,0.0),0.1f,Vec3(0,0,0));
    scene.addSphere( Vec3(0.1,0.0,0.0),0.1f,Vec3(1,0,0));
    scene.addSphere( Vec3(0,0.1,0.0),0.1f,Vec3(0,1,0));
    scene.addSphere( Vec3(0,0.0,0.1),0.1f,Vec3(0,0,1));
}

int main (int argc, char ** argv) {
    std::cout << "start" << std::endl;
    if (argc > 2) {
        printUsage ();
        exit (EXIT_FAILURE);
    }
    glutInit (&argc, argv);
    glutInitDisplayMode (GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize (SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow ("gMini");
    glewExperimental = GL_TRUE;
    glewInit (); // init glew, which takes in charges the modern OpenGL calls (v>1.2, shaders, etc)
    scene.camera.resize (SCREENWIDTH, SCREENHEIGHT);
    scene.light.color = Vec3(1.0,1.0,1.0);
    scene.light.position = Vec3(-2, 1.5, 1.5);
    scene.light.radius = 2;
    scene.backgroundColor = Vec3(2.0f/255.0f, 19.0f/255.0f, 45.0f/255.0f);
    

    //bigDemo();
    //objMeshDemo();
    //textureMappingDemo();
    //shadeRefractDemo();
    refractionDemo();
    scene.init ();
    glutIdleFunc (idle);
    glutDisplayFunc (display);
    glutKeyboardFunc (key);
    glutReshapeFunc (reshape);
    glutMotionFunc (motion);
    glutMouseFunc (mouse);
    key ('?', 0, 0);

    
    glutMainLoop ();
    return EXIT_SUCCESS;
}