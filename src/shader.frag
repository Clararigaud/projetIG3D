#version 120

//const float attenuationConstant = 0.8;
//const float attenuationLinear = 0.8;
//const float attenuationQuadratic = 0.8;

uniform vec3 inputLightPosition;
//uniform vec4 inputLightMaterial;
uniform bool lightIsInCamSpace;

uniform vec4 inputObjectAmbiantMaterial ;
uniform vec4 inputObjectDiffuseMaterial ;
uniform vec4 inputObjectSpecularMaterial ;

uniform float ambiantComposant;
uniform float diffuseComposant;
uniform float specularComposant;
uniform float inputObjectShininess ;


uniform bool activeLights[4];

uniform mat4 modelviewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

varying vec4 P; // Interpolated fragment-wise position
varying vec3 N; // Interpolated fragment-wise normal

void main (void) {
    // PUT EVERY QUANTITY IN CAMERA SPACE!
    vec4 PModelview = modelviewMatrix * P;
    vec3 position = vec3 (PModelview.xyz / PModelview.w);
    vec3 normal = normalize(normalMatrix * N);
    vec3 V = normalize(-position);

    vec3 lightPos = inputLightPosition;
    if( !lightIsInCamSpace ) {
        vec4 LModelview = modelviewMatrix * vec4(lightPos.xyz,1.0);
        lightPos = vec3 (LModelview.xyz) / LModelview.w;
    }
    vec3 L = normalize(lightPos - position);

    vec3 R = normalize( - L + 2.0*dot(L , normal) * normal );

    float distToLight = length(lightPos - position);
    float attenuation = clamp((0.2-distToLight),0.0,1.0); 
    float shade = min(1., max(0., dot(L*attenuation , normal)));
    vec4 Ia = ambiantComposant*inputObjectAmbiantMaterial;
    vec4 Id = diffuseComposant*shade*inputObjectDiffuseMaterial;
    vec4 Is = specularComposant*pow( max(dot(R , V) , 0.0), inputObjectShininess )*inputObjectSpecularMaterial;
    
    gl_FragColor = clamp((Ia + Id + Is), 0., 1.);
}