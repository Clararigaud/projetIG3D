#version 120

varying vec4 P; // Per-vertex position
varying vec3 N; // Per-vertex normal
varying vec4 C; // Per-vertex color

uniform mat4 modelviewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;
uniform float time;

#define t (time/1000.)

mat2 rot (float angle) {
    float c = cos(angle), s = sin(angle);
    return mat2(c, -s, s, c);
}

// hash based 3d value noise
// function taken from https://www.shadertoy.com/view/XslGRr
// Created by inigo quilez - iq/2013
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
float hash( float n ) { return fract(sin(n)*43758.5453); }
float noiseIQ( vec3 x ) {
  vec3 p = floor(x);
  vec3 f = fract(x);
  f       = f*f*(3.0-2.0*f);
  float n = p.x + p.y*57.0 + 113.0*p.z;
  return mix(mix(mix( hash(n+0.0), hash(n+1.0),f.x),
   mix( hash(n+57.0), hash(n+58.0),f.x),f.y),
  mix(mix( hash(n+113.0), hash(n+114.0),f.x),
   mix( hash(n+170.0), hash(n+171.0),f.x),f.y),f.z);
}

vec3 displace (vec3 pos, vec3 normal) {
//     pos.y += cos(t + pos.z);
//     pos.xz *= rot(length(pos) * 2. + t);
    float noise = max(0.0, noiseIQ(pos.xyz * 3. + vec3(t)) -0.5)*2.;
    pos.xyz += normalize(normal) * noise * .5;
    float lod = 4.;
    pos.xyz = mix(pos.xyz, floor(pos.xyz * lod) / lod, 1.-noise);
    return pos;
}

void main(void) {
    P = gl_Vertex;
    N = gl_Normal;
   // P.xyz = displace(P.xyz, N);
    vec3 weast = normalize(cross(N, vec3(0,1,0)));
    vec3 nouth = normalize(cross(N, weast));
    N = normalize(cross(normalize(displace(P.xyz + weast, N) - displace(P.xyz - weast, N)), normalize(displace(P.xyz + nouth, N) - displace(P.xyz - nouth, N))));
    vec4 p = projectionMatrix * modelviewMatrix * P;
    // gl_Position = vec4(p.xyz / p.w, 1); // this gives an error !
    gl_Position = p;
    N = gl_Normal;
}
