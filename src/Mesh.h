#ifndef MESH_H
#define MESH_H


#include <vector>
#include <string>
#include "Vec3.h"
#include "Triangle.h"
#include <GL/glut.h>
#include "src/linearSystem.h"
// -------------------------------------------
// Basic Mesh class
// -------------------------------------------

struct MeshVertex 
{
    inline MeshVertex () {}
    inline MeshVertex (const Vec3 & _p, const Vec3 & _n, double _u, double _v) : p (_p), n (_n) , u(_u), v(_v){}
    inline MeshVertex (const MeshVertex & vert) : p (vert.p), n (vert.n), u(vert.u), v(vert.v) {}
    inline virtual ~MeshVertex () {}
    inline MeshVertex & operator = (const MeshVertex & vert) {
        p = vert.p;
        n = vert.n;
        u = vert.u;
        v = vert.v;
        return (*this);
    }
    // membres :
    Vec3 p; // une position
    Vec3 n; // une normale
    double u, v;
};

struct MeshTriangle {
    inline MeshTriangle () {
        v[0] = v[1] = v[2] = 0;
    }
    inline MeshTriangle (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
    }
    inline MeshTriangle (unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;   v[1] = v1;   v[2] = v2;
    }
    inline virtual ~MeshTriangle () {}
    inline MeshTriangle & operator = (const MeshTriangle & t) {
        v[0] = t.v[0];   v[1] = t.v[1];   v[2] = t.v[2];
        return (*this);
    }
    // membres :
    unsigned int v[3];
};

struct Texture{
    std::string texFile;
    unsigned char * colorMapImage ;
    std::string colorMapImageUrl;
    unsigned int colorMapImageWidth;
    unsigned int colorMapImageHeight;
    GLuint colorTexture_handleIndex;
    GLuint colorTexture_bindingIndex;
};

struct Transparency{
    bool on = false;
    float IOR = 1.3;
};

struct Material{
    Vec3 diffuseColor = Vec3(1.0,0.5,0.5);
    Vec3 ambiant= Vec3(0.1, 0.2, 0.1);
    Vec3 specular = Vec3(0.5,0.2,1.0);
    float shininess= 10.0;
    float ka= 0.3;
    float kd = 0.8;
    float ks=0.1;
    float kr = 0.0;
    bool mirror = false;
    bool hasTexture = false;
    Texture texture;
    Transparency transparency;
};

struct IntersectionWithScene{
    bool intersectionExist;
    Vec3 p;
    Vec3 n;
    float uv[2];
    Material mat;
};

class Mesh {
public:
    std::vector<MeshVertex> V;
    std::vector<MeshTriangle> T;

    std::vector< float > positionsArray;
    std::vector< float > normalsArray;
    std::vector< unsigned int > trianglesArray;
    std::vector<float> texturesArray;	
    std::string filename;
    Material material;


    Vec3 meshposition;
    float scale;
    Mesh(const std::string & f)
    {
        filename = f;
        meshposition = Vec3(0.0,0.0,0.0);
        scale = 1.0f;
       // Mesh::createCheckerBoardImage();
    }

    Mesh() {
       // Mesh::createCheckerBoardImage();

    }
    void loadOBJ (const std::string & filename);
    void loadOFF (const std::string & filename);
    void recomputeNormals ();
    void centerAndScaleToUnit ();
    void scaleUnit ();

    void buildPositionsArray() {
        positionsArray.resize( 3 * V.size() );
        for( unsigned int v = 0 ; v < V.size() ; ++v ) {
            positionsArray[3*v + 0] = V[v].p[0];
            positionsArray[3*v + 1] = V[v].p[1];
            positionsArray[3*v + 2] = V[v].p[2];
            // std::cout << "buildPositionsArray " << positionsArray[3*v + 0] << " " << positionsArray[3*v + 1] << " " << positionsArray[3*v + 2] << std::endl;
        }
    }
    
    void buildNormalsArray() {
        normalsArray.resize( 3 * V.size() );
        for( unsigned int v = 0 ; v < V.size() ; ++v ) {
            normalsArray[3*v + 0] = V[v].n[0];
            normalsArray[3*v + 1] = V[v].n[1];
            normalsArray[3*v + 2] = V[v].n[2];
        }
    }
    
    void buildTrianglesArray() {
        trianglesArray.resize( 3 * T.size() );
        for( unsigned int t = 0 ; t < T.size() ; ++t ) {
            trianglesArray[3*t + 0] = T[t].v[0];
            trianglesArray[3*t + 1] = T[t].v[1];
            trianglesArray[3*t + 2] = T[t].v[2];
            // std::cout << "buildTrianglesArray " << trianglesArray[3*t + 0] << " " << trianglesArray[3*t + 1] << " " << trianglesArray[3*t + 2] << std::endl;
        }
    }

    void buildTextureCoordArray() {
        texturesArray.resize( 2 * V.size() );
        for( unsigned int v = 0 ; v < V.size() ; ++v ) {
            texturesArray[2*v + 0] = V[v].u;
            texturesArray[2*v + 1] = V[v].v;
            std::cout << "buildTextureCoordArray " << texturesArray[2*v + 0] << " " << texturesArray[2*v + 1]<< std::endl;
        }
    }
    
    void draw()  {
        if( trianglesArray.size() == 0 ) return;
        if(material.hasTexture)
        {        glEnableClientState (GL_TEXTURE_COORD_ARRAY);
                glTexCoordPointer(3, GL_FLOAT, 2*sizeof (float), (GLvoid*)(texturesArray.data()));
        }
        glEnableClientState(GL_VERTEX_ARRAY) ;
        glEnableClientState (GL_NORMAL_ARRAY);

        glNormalPointer (GL_FLOAT, 3*sizeof (float), (GLvoid*)(normalsArray.data()));
        glVertexPointer (3, GL_FLOAT, 3*sizeof (float) , (GLvoid*)(positionsArray.data()));
        glDrawElements(GL_TRIANGLES, trianglesArray.size(), GL_UNSIGNED_INT, (GLvoid*)(trianglesArray.data()));
    }

    void createCheckerBoardImage( ) {
        material.texture.colorMapImage = new unsigned char[ 4 * material.texture.colorMapImageWidth * material.texture.colorMapImageHeight ];
        for(unsigned int i = 0 ; i < material.texture.colorMapImageWidth ; ++i) {
            for(unsigned int j=0 ; j < material.texture.colorMapImageHeight ; ++j) {
         char value = 0;
        if(((2*i)/material.texture.colorMapImageWidth + (2*j)/material.texture.colorMapImageHeight) %2) { value = 255; }
        material.texture.colorMapImage[4 * (i*material.texture.colorMapImageHeight+j) +0] = value; // red
         material.texture.colorMapImage[4 * (i*material.texture.colorMapImageHeight+j) +1] = value; // green
         material.texture.colorMapImage[4 * (i*material.texture.colorMapImageHeight+j) +2] = value; // blue
         material.texture.colorMapImage[4 * (i*material.texture.colorMapImageHeight+j) +3] = 255;   // alpha ( = 1 )
            }
        }
    }

    Vec3 getLinearityCoefficients(Vec3 p, Vec3 c0, Vec3 c1, Vec3 c2){
        linearSystem mySystem;
        mySystem.setDimensions(4 , 3);

        mySystem.A(0,0) = c0[0];  mySystem.A(0,1) = c1[0]; mySystem.A(0,2) = c2[0];
        mySystem.A(1,0) = c0[1];  mySystem.A(1,1) = c1[1]; mySystem.A(1,2) = c2[1];
        mySystem.A(2,0) = c0[2];  mySystem.A(2,1) = c1[2]; mySystem.A(2,2) = c2[2];
        // the values that are not set with mySystem.A(row,column) = value, are set to 0 by default.
        mySystem.b(0) = p[0] ;
        mySystem.b(1) = p[1] ;
        mySystem.b(2) = p[2] ;

        mySystem.preprocess();
        Eigen::VectorXd X;
        mySystem.solve(X);

        return Vec3(X[0], X[1] ,X[2]);
    }

    IntersectionWithScene intersection(Ray const & ray){ // ray = direct et origine
		IntersectionWithScene res;
		res.intersectionExist = false;
        
        RayTriangleIntersection rest;
        float lambdamin = 100000000000000;

        for( unsigned int t = 0 ; t < T.size() ; ++t ) { // on parcours tous les triangles
            Vec3 c0 = V[T[t].v[0]].p;
            Vec3 c1 = V[T[t].v[1]].p;
            Vec3 c2 = V[T[t].v[2]].p;
            Triangle triangle =  Triangle( c0 , c1 ,c2 );
            rest = triangle.getIntersection(ray);
            if(rest.intersectionExists && rest.lambda< lambdamin ){
                res.intersectionExist = true;
                res.p = rest.intersection;
                res.n = rest.n;
                res.n.normalize();
                lambdamin = rest.lambda;
                res.mat = material;
                Vec3 coefs = getLinearityCoefficients(res.p, c0, c1, c2);
                res.uv[0] = coefs[0]*V[T[t].v[0]].u +  coefs[1]*V[T[t].v[1]].u + coefs[2]*V[T[t].v[2]].u;
                res.uv[1] = coefs[0]*V[T[t].v[0]].v +  coefs[1]*V[T[t].v[1]].v + coefs[2]*V[T[t].v[2]].v;
			}
        }
		return res;
    }

};
#endif