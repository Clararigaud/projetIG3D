#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include "src/matrixUtilities.h"
#include "Ray.h"
#include "Camera.h"
#include <GL/glut.h>
#include "Mesh.h"
#include "Sphere.h"

using namespace std;

struct Light{
    Vec3 position;
    Vec3 color;
    Vec3 direction;
    double radius; // if you want the light to be a sphere (more realistic)
};

class Scene {
    std::vector< Mesh > meshes;
    std::vector< Sphere > spheres;
    GLProgram * glProgram;
    GLProgram * glProgramTextures;
    GLuint countTextures;
    std::vector<GLuint> textures;
public:
    Camera camera;
    Light light;
    Vec3 backgroundColor;
    float time;
    Scene() {
		
    }

    void addSphere(Sphere s){
		spheres.push_back(s);
	    Sphere & sphereAjoutee = spheres[ spheres.size() - 1 ];
		sphereAjoutee.setSphere(s.patch_h,s.patch_v);
	    sphereAjoutee.buildPositionsArray();
	    sphereAjoutee.buildNormalsArray();
	    sphereAjoutee.buildTrianglesArray();
    }

    void addSphere( Vec3 const & center , float radius, Vec3 material=Vec3(0,0,0), unsigned int patch_h=100, unsigned int patch_v =100) {
		Sphere s = Sphere(center, radius);
		s.material.diffuseColor = material;
		spheres.push_back(s);
        Sphere & sphereAjoutee = spheres[ spheres.size() - 1 ];
        sphereAjoutee.setSphere(patch_h,patch_v);
        sphereAjoutee.buildPositionsArray();
        sphereAjoutee.buildNormalsArray();
        sphereAjoutee.buildTrianglesArray();
        //sphereAjoutee.buildTextureCoordArray();
    }

    void addMesh(std::string const & modelFilename) {
		Mesh m = Mesh(modelFilename);
		meshes.push_back(m);
		Mesh & meshAjoute = meshes[meshes.size() - 1];
		loadMesh(meshAjoute);
    }

 	void addMesh(Mesh m) {
		meshes.push_back(m);
		Mesh & meshAjoute = meshes[meshes.size() - 1];
		loadMesh(meshAjoute);
    }

    void loadMesh(Mesh & meshAjoute){
    	std::size_t pos = meshAjoute.filename.find(".");

		if(meshAjoute.filename.substr(pos)==".obj"){
			meshAjoute.loadOBJ (meshAjoute.filename);
			meshAjoute.recomputeNormals ();
		}
		else if(meshAjoute.filename.substr(pos)==".off"){
	        meshAjoute.loadOFF (meshAjoute.filename);
	        meshAjoute.recomputeNormals ();
		}else{
			printf("Extension de fichier inconnue\n");
            exit(EXIT_FAILURE);
		}
        meshAjoute.buildPositionsArray();
        meshAjoute.buildNormalsArray();
        meshAjoute.buildTrianglesArray();
        if(meshAjoute.material.hasTexture){
        	//textures.resize(textures.size()+1);
        	meshAjoute.buildTextureCoordArray();
        	meshAjoute.material.texture.colorTexture_bindingIndex = countTextures;
        	ppmLoader::load_ppm(meshAjoute.material.texture.colorMapImage , meshAjoute.material.texture.colorMapImageWidth,meshAjoute.material.texture.colorMapImageHeight, meshAjoute.material.texture.colorMapImageUrl);
        	glEnable(GL_TEXTURE_2D);
        	glActiveTexture(GL_TEXTURE0+meshAjoute.material.texture.colorTexture_bindingIndex);	
        	glGenTextures(1 , &meshAjoute.material.texture.colorTexture_handleIndex);
		    glBindTexture(GL_TEXTURE_2D , meshAjoute.material.texture.colorTexture_handleIndex);

		    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		    glTexImage2D(GL_TEXTURE_2D , 0 , GL_RGB , meshAjoute.material.texture.colorMapImageWidth , meshAjoute.material.texture.colorMapImageHeight , 0 , GL_RGB , GL_UNSIGNED_BYTE , meshAjoute.material.texture.colorMapImage);
		   
		    glDisable(GL_TEXTURE_2D);
        	countTextures +=1;


        }
    }

    void init () {
		glCullFace (GL_BACK);
		glEnable (GL_CULL_FACE);
		glDepthFunc (GL_LESS);
		glEnable (GL_DEPTH_TEST);

		countTextures = 1;
		glClearColor (backgroundColor[0], backgroundColor[1], backgroundColor[2], 1.0f);
		std::cout << "Load shaders" << std::endl;

		// Load first shader
		try {
		    glProgram = GLProgram::genVFProgram ("Simple GL Program", "./src/shader.vert", "./src/shader.frag");
		    // Load and compile pair of shaders
		    glProgram->use ();
		    glProgram->stop ();
		    // Activate the shader program
		} catch (Exception & e) {
		    cerr << e.msg () << endl;
		}

		std::cout << "Load shaders with textures" << std::endl;
	    
		// Load shader with textures
		try {
		    glProgramTextures = GLProgram::genVFProgram ("Simple GL Program", "./src/shader2.vert", "./src/shader2.frag");
		    // Load and compile pair of shaders
		    glProgramTextures->use ();
		    glProgramTextures->stop ();
		    // Activate the shader program
		} catch (Exception & e) {
		    cerr << e.msg () << endl;
		}
    }
    
    void draw() const {
		GLfloat mvmatrix[16];
		GLfloat nmatrix[9];
		GLfloat pjmatrix[16];
		glMatrixMode(GL_PROJECTION);
		glGetFloatv (GL_PROJECTION_MATRIX,pjmatrix);
		glMatrixMode(GL_MODELVIEW);
		glGetFloatv (GL_MODELVIEW_MATRIX, mvmatrix);
		
		for( int i = 0 ; i < 3 ; ++i ) {
		    for( int j = 0 ; j < 3 ; ++j ) {
			nmatrix[3*i+j] = mvmatrix[4*i+j];
		    }
		}
	
		for( unsigned int sIt = 0 ; sIt < (meshes.size()+spheres.size()) ; ++sIt ) {
			Mesh  mesh;

			if(sIt<meshes.size()){
			    mesh = meshes[sIt];
			}else{
			    mesh = spheres[sIt-meshes.size()];  
			}

		    if(mesh.material.hasTexture){
		        glProgramTextures->use();
	            {
		     		//std::cout << mesh.material.texture.colorTexture_bindingIndex << std::endl;
	             	glProgramTextures->setUniform1i( "uTextureColor" , mesh.material.texture.colorTexture_bindingIndex ); 
            		//glProgramTextures->setUniform1i( "uTextureColor" , 0 ); 
		  	 		// glProgramTextures->setUniform1i( "uTextureNormal" , normalTexture_bindingIndex ); ---------------->>>> NORMAL MAP LATER
	             	//std::cout << "uTexturesColor" << std::endl;
	             	glProgramTextures->setUniform3f("inputLightPosition" , light.position[0], light.position[1], light.position[2]);
	             	//std::cout << "inputLightPosition" << std::endl;
	            	//glProgramTextures->setUniform4f("inputLightMaterial" , 1.0, 1.0, 1.0 , 1.0);
	             	//std::cout << "inputLightMaterial" << std::endl;
	             	glProgramTextures->setUniform1i("lightIsInCamSpace" , 0);
					// //std::cout << "lightIsInCamSpace" << std::endl;
					 glProgramTextures->setUniform4f("inputObjectDiffuseMaterial" ,mesh.material.diffuseColor[0], mesh.material.diffuseColor[1], mesh.material.diffuseColor[2] ,  1.0);
					// //std::cout << "Setup inputObjectDiffuseMaterial" << std::endl;
					 glProgramTextures->setUniform4f("inputObjectSpecularMaterial" ,mesh.material.specular[0], mesh.material.specular[1], mesh.material.specular[2]  , 1.0);
		            //	std::cout << "inputObjectSpecularMaterial" << std::endl;
					////std::cout << "Setup ambiantComposant" << std::endl;
					glProgramTextures->setUniform1f("ambiantComposant" , mesh.material.ka);
					////std::cout << "Setup diffuseComposant" << std::endl;
					glProgramTextures->setUniform1f("diffuseComposant" , mesh.material.kd);
					////std::cout << "Setup specularComposant" << std::endl;
					glProgramTextures->setUniform1f("specularComposant" , mesh.material.ks);
	             	glProgramTextures->setUniform1f("inputObjectShininess" , mesh.material.shininess);
					// //std::cout << "inputObjectShininess" << std::endl;
	             	glProgramTextures->setUniformMatrix4fv("modelviewMatrix",mvmatrix);
	             	//std::cout << "modelviewMatrix" << std::endl;
	            	glProgramTextures->setUniformMatrix4fv("projectionMatrix",pjmatrix);
	             	//std::cout << "projectionMatrix" << std::endl;
	            	glProgramTextures->setUniformMatrix3fv("normalMatrix",nmatrix);
	            	//std::cout << "GL Program Textures Done" << std::endl;
	            }
 				
		    }else{

			    glProgram->use();
			    {
				// std::cout << "Setup uniforms in shaders" << std::endl;
				// std::cout << "Setup inputLightPosition" << std::endl;
				glProgram->setUniform3f("inputLightPosition" , light.position[0], light.position[1], light.position[2]);
				//glProgram->setUniform4f("inputLightMaterial" , light.color[0], light.color[1], light.color[2], light.radius);
				
				////std::cout << "Setup lightIsInCamSpace" << std::endl;
				glProgram->setUniform1i("lightIsInCamSpace" , 0);
				
				////std::cout << "Setup time" << std::endl;
				//glProgram->setUniform1f("time",time);
				
				////std::cout << "Setup inputObjectAmbiantMaterial" << mesh.material.ambiant << std::endl;
				glProgram->setUniform4f("inputObjectAmbiantMaterial" , mesh.material.ambiant[0], mesh.material.ambiant[1], mesh.material.ambiant[2] , 1.0);
				
				////std::cout << "Setup inputObjectDiffuseMaterial" << std::endl;
				glProgram->setUniform4f("inputObjectDiffuseMaterial" ,mesh.material.diffuseColor[0], mesh.material.diffuseColor[1], mesh.material.diffuseColor[2] ,  1.0);
				
				////std::cout << "Setup inputObjectSpecularMaterial" << std::endl;
				glProgram->setUniform4f("inputObjectSpecularMaterial" ,mesh.material.specular[0], mesh.material.specular[1], mesh.material.specular[2]  , 1.0);
				
				////std::cout << "Setup ambiantComposant" << std::endl;
				glProgram->setUniform1f("ambiantComposant" , mesh.material.ka);
				////std::cout << "Setup diffuseComposant" << std::endl;
				glProgram->setUniform1f("diffuseComposant" , mesh.material.kd);
				////std::cout << "Setup specularComposant" << std::endl;
				glProgram->setUniform1f("specularComposant" , mesh.material.ks);
				
				////std::cout << "Setup inputObjectShininess" << std::endl;
				glProgram->setUniform1f("inputObjectShininess" , mesh.material.shininess);
				 
				glProgram->setUniformMatrix4fv("modelviewMatrix",mvmatrix);
				////std::cout << "Setup modelviewMatrix" << std::endl;
				glProgram->setUniformMatrix4fv("projectionMatrix",pjmatrix);
				////std::cout << "Setup projectionMatrix" << std::endl;
				glProgram->setUniformMatrix3fv("normalMatrix",nmatrix);
				////std::cout << "Setup uniforms in shaders OK" << std::endl;
			    }	    
		    }

	        mesh.draw();

	        if(mesh.material.hasTexture){
	        	glProgramTextures->stop();
	        }
            else{
            	glProgram->stop();}
	        
		}
    }


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                     RAYTRACING                                                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
    
    void rayTraceFromCamera() {
		int w = glutGet(GLUT_WINDOW_WIDTH)  ,   h = glutGet(GLUT_WINDOW_HEIGHT);
		std::cout << "Ray tracing a " << w << " x " << h << " image" << std::endl;
		camera.apply();
		Vec3 pos , dir;
    	//unsigned int nsamples = 100;
		unsigned int nsamples = 20;
		std::vector< Vec3 > image( w*h , Vec3(0,0,0) );
		
		for (int y=0; y<h; y++){
			for (int x=0; x<w; x++) {
			for( unsigned int s = 0 ; s < nsamples ; ++s ) {
				float u = ((float)(x) + (float)(rand())/(float)(RAND_MAX)) / w;
				float v = ((float)(y) + (float)(rand())/(float)(RAND_MAX)) / h;
				// this is a random uv that belongs to the pixel xy.
				screenSpaceToWorldSpaceRay(u,v,pos,dir);
				Vec3 color = rayTrace( Ray(pos , dir),5); // add the parameters that you want (maximum number of bounds for example)	
				image[x + y*w] += color;
			}
			image[x + y*w] /= nsamples;
			}
		}
		std::cout << "\tRay tracing is done" << std::endl;

		std::string filename = "./myImage.ppm";	
		ofstream f(filename.c_str(), ios::binary);
		if (f.fail()) {
			cout << "Could not open file: " << filename << endl;
			return;
		}
		f << "P3" << std::endl << w << " " << h << std::endl << 255 << std::endl;
		for (int i=0; i<w*h; i++)
			f << (int)(255.f*std::min<float>(1.f,image[i][0])) << " " << (int)(255.f*std::min<float>(1.f,image[i][1])) << " " << (int)(255.f*std::min<float>(1.f,image[i][2])) << " ";
		f << std::endl;
		f.close();
    }
    
    Vec3 rayTrace( Ray const & ray , unsigned int nbRebonds = 10 ) {
		Vec3 color(0,0,0);
		Vec3 brdf;
		IntersectionWithScene sceneIntersect = intersect(ray);
		if(sceneIntersect.intersectionExist){
		// get a random sample on the sphere:
	        Vec3 randVectorInSphere( -1.0 + 2.0 * (float)rand()/(float)(RAND_MAX), -1.0 + 2.0 * (float)rand()/(float)(RAND_MAX), -1.0 + 2.0 * (float)rand()/(float)(RAND_MAX)  );
	        randVectorInSphere.normalize();
			Vec3 lightPos = light.position + light.radius * randVectorInSphere; // pointLight	
			Vec3 V = (ray.origin() - sceneIntersect.p); V.normalize(); // w_out
			Vec3 L = (lightPos - sceneIntersect.p); L.normalize();//w_in
			Vec3 N = sceneIntersect.n;

			Vec3 R = 2*(Vec3::dot(L,N))*N - L; // rayon parfaitement reflechi
			if(!sceneIntersect.mat.mirror && !sceneIntersect.mat.transparency.on ){
		    	Ray rayFromPointToLight( sceneIntersect.p + 0.0001 *L , L ); 
		       	IntersectionWithScene intersectionLightRayWithScene = intersect(rayFromPointToLight);
				
		        if( ! intersectionLightRayWithScene.intersectionExist   ||   (intersectionLightRayWithScene.p - sceneIntersect.p).squareLength() > (lightPos - sceneIntersect.p).squareLength() ){
				    // si il n'y a rien entre le point et la source de lumière 
					color += Vec3::componentWiseProduct( light.color , computePhong(sceneIntersect, V, L , N , R ) );
					if(nbRebonds>0){
						Vec3 randVectorInHemisphere( -1.0 + 2.0 * (float)rand()/(float)(RAND_MAX), -1.0 + 2.0 * (float)rand()/(float)(RAND_MAX), -1.0 + 2.0 * (float)rand()/(float)(RAND_MAX)  );
						if( Vec3::dot(randVectorInHemisphere , N) < 0.0 ){
							randVectorInHemisphere = -1.0 * randVectorInHemisphere;
						}
						randVectorInHemisphere.normalize();
						Ray const rayRebond(sceneIntersect.p + 0.00001 * randVectorInHemisphere, randVectorInHemisphere);
						Vec3 R2 = 2*(Vec3::dot(randVectorInHemisphere,N))*N - randVectorInHemisphere; // rayon reflechi random à partir du point d'intersection
						color += Vec3::componentWiseProduct( rayTrace(rayRebond, nbRebonds - 1) , computePhong(sceneIntersect, V, randVectorInHemisphere , N , R2 ) );
					}
				}
			}
			else{
				float kr =sceneIntersect.mat.kr;
				Vec3 L = (light.position - sceneIntersect.p); L.normalize();//w_in
				Vec3 N = sceneIntersect.n;
				Vec3 Rref = 2*(Vec3::dot(L,N))*N - L; // rayon parfaitement reflechi

				
				if(sceneIntersect.mat.mirror){
				 	if(nbRebonds>0){
				        Ray const rayRebond(sceneIntersect.p, R);
				        color = kr*rayTrace(rayRebond, nbRebonds - 1);
				 	}
				}
				if(sceneIntersect.mat.transparency.on){
				 	if(nbRebonds>0){
				 	Vec3 Valea = Vec3(V[0] * (-0.01 + 0.02 * (float)rand()/(float)(RAND_MAX)) , V[1] * (-0.01 + 0.02 * (float)rand()/(float)(RAND_MAX) ),V[2] * (-0.01 + 0.02* (float)rand()/(float)(RAND_MAX)));
				    float cosi = max(-1.0, min(1.0, (double)Vec3::dot(Valea, N))); 
				    float etai = 1, etat = sceneIntersect.mat.transparency.IOR; 	
				    Vec3 n = N; 
				    if (cosi < 0) { cosi = -cosi; } else { std::swap(etai, etat); n= Vec3(0.,0.,0.)-N; } 
				    float eta = etai / etat; 
				    float k = 1 - eta * eta * (1 - cosi * cosi);
				    Vec3 Rrefr = eta * Valea + (eta * cosi - sqrtf(k)) * n ; 
       				Ray const rayRebond(sceneIntersect.p, R);
				    Ray const rayThrough(sceneIntersect.p, Rrefr);
				    color = (1-kr)*rayTrace(rayThrough, nbRebonds - 1)+kr*rayTrace(rayRebond, nbRebonds - 1);
					}
				}
			}
			return color;
		}
		else{
			return backgroundColor;
		}
    }
    
    Vec3 getColorFromTexture(float u, float v, unsigned char *	 colorMapImage, unsigned int  width, unsigned int  height){
    	unsigned int coordX =  u * width;
    	unsigned int coordY = v * height;
    	int startInImg = coordY*width*3 + 3*coordX;
    	float r = (float) colorMapImage[startInImg +0 ]/255;
		float g = (float) colorMapImage[startInImg +1 ]/255;
		float b = (float) colorMapImage[startInImg +2 ]/255;
		return Vec3(r,g,b);
    }
    Vec3 computePhong( IntersectionWithScene & sceneIntersect, Vec3 V, Vec3 L, Vec3 N, Vec3 R){
		Vec3 color;
		float shade = Vec3::dot(L,N);
		shade = min(1.f, max(0.f, shade));


		float ka = sceneIntersect.mat.ka; // composante ambiante = proportion de lumière renvoyée
		float kd = sceneIntersect.mat.kd; // composante diffuse
		float ks = sceneIntersect.mat.ks; //composante speculaire	   
		float ns = sceneIntersect.mat.shininess;
		Vec3 ia = sceneIntersect.mat.ambiant; // intensité lumière incidente ambiante
		Vec3 id = sceneIntersect.mat.diffuseColor; // intensité lumière incidente diffuse
		Vec3 is = sceneIntersect.mat.specular; // intensité lumière incidente spéculaire 
		
		Vec3 Ia = ka*ia;
		Vec3 Id = kd*shade*id;
		Vec3 Is = ks*pow( max(Vec3::dot(R,V),0.0f),ns)*is;
		color = Ia + Id + Is;
		if(sceneIntersect.mat.hasTexture){
			Vec3 colorInTexture = getColorFromTexture(sceneIntersect.uv[0], sceneIntersect.uv[1], sceneIntersect.mat.texture.colorMapImage, sceneIntersect.mat.texture.colorMapImageWidth, sceneIntersect.mat.texture.colorMapImageHeight);
			color = Vec3::componentWiseProduct(color,colorInTexture);
		}

		return color ;
    };

    IntersectionWithScene intersect(Ray const & ray){
		IntersectionWithScene res;
		res.intersectionExist = false;
		double lambdaMin = 100000000; //big value on va regarder le lambda minimum pour savoir qulle sphere est devant // intersectée en premier
		for (unsigned int s = 0; s < (meshes.size()+spheres.size()) ; ++s) {
		    IntersectionWithScene inte;
		    if(s<meshes.size()){
			Mesh & mesh = meshes[s];
			 inte =  mesh.intersection(ray); // on recupère la distance cam - mesh 
		    }else{
			Sphere & mesh = spheres[s-meshes.size()];  
			 inte =  mesh.intersection(ray); // on recupère la distance cam - mesh 
		    }
		   
			if (inte.intersectionExist){
				double lambda = (inte.p-ray.origin()).length();
				////std::cout << "intersection found" <<inte.p << std::endl;
				if(lambda > 0.0 && lambda < lambdaMin){
					lambdaMin = lambda;
					res = inte;
				}
			}
		}	
		res.intersectionExist = lambdaMin < 100000000;
		return res;
    }
};
#endif