#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <string>
#include "Mesh.h"
#include "Sphere.h"
#include "Ray.h"
#include "Camera.h"
#include <GL/glut.h>

#include "src/matrixUtilities.h"
using namespace std;

struct Light{
    Vec3 position;
    Vec3 color;
    Vec3 direction;
    double radius; // if you want the light to be a sphere (more realistic)
};


class Scene {
    std::vector< Mesh > meshes;
    std::vector< Sphere > spheres;
    
    GLProgram * glProgram;
    GLProgram * glProgramTextures;

    unsigned char * colorMapImage;
    unsigned int colorMapImageWidth = 256;
    unsigned int colorMapImageHeight = 256;
    GLuint colorTexture_handleIndex;
    GLuint colorTexture_bindingIndex;

    unsigned char * normalMapImage;
    unsigned int normalMapImageWidth = 256;	
    unsigned int normalMapImageHeight = 256;
    GLuint normalTexture_handleIndex;
    GLuint normalTexture_bindingIndex;
    GLuint normalTextureFormat = GL_RGB;

public:
    Camera camera;
    Light light;
    Scene() {
	
    }
    
    void addSphere( Vec3 const & center , float radius, Vec3 material=Vec3(0,0,0), unsigned int patch_h=10, unsigned int patch_v =10) {
        
	Sphere s = Sphere(center, radius);
	s.material.diffuseColor = material;
	spheres.push_back(s);
        Sphere & sphereAjoutee = spheres[ spheres.size() - 1 ];
        sphereAjoutee.setSphere(patch_h,patch_v);
        sphereAjoutee.buildPositionsArray();
        sphereAjoutee.buildNormalsArray();
        sphereAjoutee.buildTrianglesArray();
        //sphereAjoutee.buildTextureCoordArray();
    }

    void addMesh(std::string const & modelFilename) {
        meshes.resize( meshes.size() + 1 );
        Mesh & meshAjoute = meshes[ meshes.size() - 1 ];

        meshAjoute.loadOFF (modelFilename);
        meshAjoute.centerAndScaleToUnit ();
        meshAjoute.recomputeNormals ();
        meshAjoute.buildPositionsArray();
        meshAjoute.buildNormalsArray();
        meshAjoute.buildTrianglesArray();
    }
    
    void init () {

	glCullFace (GL_BACK);
	glEnable (GL_CULL_FACE);
	glDepthFunc (GL_LESS);
	glEnable (GL_DEPTH_TEST);
	glClearColor (0.2f, 0.2f, 0.3f, 1.0f);

	// load images:
	createCheckerBoardImage(); // on crée un tableau RGB*w*h
	ppmLoader::load_ppm(normalMapImage , normalMapImageWidth , normalMapImageHeight , "./img/normalMaps/n3.ppm" , ppmLoader::rbg);

	glEnable(GL_TEXTURE_2D);
	colorTexture_bindingIndex = 0; // index de cette texture dans le tab textures (mémoire)
	glActiveTexture(GL_TEXTURE0 + colorTexture_bindingIndex); 
	glGenTextures(1, & colorTexture_handleIndex); 
	glBindTexture(GL_TEXTURE_2D, colorTexture_handleIndex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER , GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER , GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S , GL_REPEAT );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T , GL_REPEAT );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA , colorMapImageWidth, colorMapImageHeight , 0 ,GL_RGBA , GL_UNSIGNED_BYTE , colorMapImage);// charge la texture sur le GPU

	glGenerateMipmap(GL_TEXTURE_2D); 

    // normal texture
	normalTexture_bindingIndex = 1; // index de cette texture dans le tab textures (mémoire)z
	glActiveTexture(GL_TEXTURE0 + normalTexture_bindingIndex); 
	glGenTextures(1, & normalTexture_handleIndex); 
	glBindTexture(GL_TEXTURE_2D, normalTexture_handleIndex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER , GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER , GL_LINEAR_MIPMAP_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S , GL_REPEAT );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T , GL_REPEAT );
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA , normalMapImageWidth, normalMapImageHeight , 0 ,normalTextureFormat , GL_UNSIGNED_BYTE , normalMapImage);// charge la texture sur le GPU
	glGenerateMipmap(GL_TEXTURE_2D); 

	glDisable(GL_TEXTURE_2D);

	// Load first shader
	try {
	    glProgram = GLProgram::genVFProgram ("Simple GL Program", "./src/shader.vert", "./src/shader.frag");
	    // Load and compile pair of shaders
	    glProgram->use ();
	    glProgram->stop ();
	    // Activate the shader program
	} catch (Exception & e) {
	    cerr << e.msg () << endl;
	}
    
	// Load shader with textures
	try {
	    glProgramTextures = GLProgram::genVFProgram ("Simple GL Program", "./src/shader2.vert", "./src/shader2.frag");
	    // Load and compile pair of shaders
	    glProgramTextures->use ();
	    glProgramTextures->stop ();
	    // Activate the shader program
	} catch (Exception & e) {
	    cerr << e.msg () << endl;
	}
    }
    
    void draw() const {
	GLfloat mvmatrix[16];
	GLfloat nmatrix[9];
	GLfloat pjmatrix[16];
	glMatrixMode(GL_PROJECTION);
	glGetFloatv (GL_PROJECTION_MATRIX,pjmatrix);
	glMatrixMode(GL_MODELVIEW);
	glGetFloatv (GL_MODELVIEW_MATRIX, mvmatrix);
	for( int i = 0 ; i < 3 ; ++i ) {
	    for( int j = 0 ; j < 3 ; ++j ) {
		nmatrix[3*i+j] = mvmatrix[4*i+j];
	    }
	}

	// set openGL parameters:
	glProgram->use();
	{
	    glProgram->setUniform3f("inputLightPosition" , light.position[0], light.position[1], light.position[2]);
	    glProgram->setUniform4f("inputLightMaterial" , light.color[0], light.color[1], light.color[2], 1.0);
	    glProgram->setUniform1i("lightIsInCamSpace" , 0);

	    glProgram->setUniform4f("inputObjectAmbiantMaterial" , 0.1, 0.2, 0.1 , 1.0);
	    glProgram->setUniform4f("inputObjectDiffuseMaterial" , 0.3, 0.3, 0.4 , 1.0);
	    glProgram->setUniform4f("inputObjectSpecularMaterial" , 1.0, 1.0, 1.0 , 1.0);
	    glProgram->setUniform1f("inputObjectShininess" , 50.0);

	    glProgram->setUniformMatrix4fv("modelviewMatrix",mvmatrix);
	    glProgram->setUniformMatrix4fv("projectionMatrix",pjmatrix);
	    glProgram->setUniformMatrix3fv("normalMatrix",nmatrix);
	}

        // iterer sur l'ensemble des objets, et faire leur rendu :
        for( unsigned int mIt = 0 ; mIt < meshes.size() ; ++mIt ) {
            Mesh const & mesh = meshes[mIt];
            mesh.draw();

//            glPushMatrix();
//            glTranslatef(2,0,0);
//            mesh.draw();
//            glPopMatrix();

//            glPushMatrix();
//            glTranslatef(-1.2,0,0);
//            glRotatef(90 , 0 , 1 , 0);
//            glScalef(0.5,0.5,0.5);
//            glEnable(GL_NORMALIZE);
//            mesh.draw();
//            glDisable(GL_NORMALIZE);
//            glPopMatrix();
        }
        for( unsigned int sIt = 0 ; sIt < spheres.size() ; ++sIt ) {
            Sphere const & sphere = spheres[sIt];
	    
	    glColor3f(sphere.material.diffuseColor[0],sphere.material.diffuseColor[1],sphere.material.diffuseColor[2]);
            sphere.draw();
        }
        glProgram->stop();
// 	glProgramTextures->use();
// 	{
// 	    //c'est cette texture qu'on envoie
// 	    glProgramTextures->setUniform1i( "uTextureColor" , colorTexture_bindingIndex ); 
// 	    glProgramTextures->setUniform1i( "uTextureNormal" , normalTexture_bindingIndex );
// 
// 	    glProgramTextures->setUniform3f("inputLightPosition" , light.position[0], light.position[1], light.position[2]);
// 	    glProgramTextures->setUniform4f("inputLightMaterial" , 1.0, 1.0, 1.0 , 1.0);
// 	    glProgramTextures->setUniform1i("lightIsInCamSpace" , 0);
// 
// 	    glProgramTextures->setUniform4f("inputObjectDiffuseMaterial" , 1.0, 1.0, 1.0 , 1.0);
// 	    glProgramTextures->setUniform4f("inputObjectSpecularMaterial" , 1.0, 1.0, 1.0 , 1.0);
// 	    glProgramTextures->setUniform1f("inputObjectShininess" , 10.0);
// 
// 	    glProgramTextures->setUniformMatrix4fv("modelviewMatrix",mvmatrix);
// 	    glProgramTextures->setUniformMatrix4fv("projectionMatrix",pjmatrix);
// 	    glProgramTextures->setUniformMatrix3fv("normalMatrix",nmatrix);
// 	}
// 	{
// 	    glBegin(GL_QUADS); // we know it is bad, but we just want a single quad for the sake of the exercise
// 	    glNormal3f(0,1,0);
// 	    glTexCoord2f(-10,10);    glVertex3f(-10,-1.5,10);
// 	    glTexCoord2f(10,10);     glVertex3f(10,-1.5,10);
// 	    glTexCoord2f(10,-10);    glVertex3f(10,-1.5,-10);
// 	    glTexCoord2f(-10,-10);   glVertex3f(-10,-1.5,-10);
// 	    glEnd();
// 	}
// 	glProgramTextures->stop();
    }
 


    void createCheckerBoardImage( ) {
	colorMapImage = new unsigned char[ 4 * colorMapImageWidth * colorMapImageHeight ];
	for(unsigned int i = 0 ; i < colorMapImageWidth ; ++i) {
	    for(unsigned int j=0 ; j < colorMapImageHeight ; ++j) {
		char value = 0;
		if(((2*i)/colorMapImageWidth + (2*j)/colorMapImageHeight) %2) { value = 255; }
		colorMapImage[4 * (i*colorMapImageHeight+j) +0] = value; // red
		colorMapImage[4 * (i*colorMapImageHeight+j) +1] = value; // green
		colorMapImage[4 * (i*colorMapImageHeight+j) +2] = value; // blue
		colorMapImage[4 * (i*colorMapImageHeight+j) +3] = 255;   // alpha ( = 1 )
	    }
	}
    }
    //RAYTRACING
    
    
    void rayTraceFromCamera() {
	int w = glutGet(GLUT_WINDOW_WIDTH)  ,   h = glutGet(GLUT_WINDOW_HEIGHT);
	std::cout << "Ray tracing a " << w << " x " << h << " image" << std::endl;
	camera.apply();
	Vec3 pos , dir;
    //    unsigned int nsamples = 100;
	unsigned int nsamples = 100;
	std::vector< Vec3 > image( w*h , Vec3(0,0,0) );
	for (int y=0; y<h; y++){
	    for (int x=0; x<w; x++) {
		for( unsigned int s = 0 ; s < nsamples ; ++s ) {
		    float u = ((float)(x) + (float)(rand())/(float)(RAND_MAX)) / w;
		    float v = ((float)(y) + (float)(rand())/(float)(RAND_MAX)) / h;
		    // this is a random uv that belongs to the pixel xy.
		    screenSpaceToWorldSpaceRay(u,v,pos,dir);
		    Vec3 color = rayTrace( Ray(pos , dir),10); // add the parameters that you want (maximum number of bounds for example)
		    image[x + y*w] += color;
		}
		image[x + y*w] /= nsamples;
	    }
	}
	std::cout << "\tRay tracing is done" << std::endl;

	std::string filename = "./myImage.ppm";	
	ofstream f(filename.c_str(), ios::binary);
	if (f.fail()) {
	    cout << "Could not open file: " << filename << endl;
	    return;
	}
	f << "P3" << std::endl << w << " " << h << std::endl << 255 << std::endl;
	for (int i=0; i<w*h; i++)
	    f << (int)(255.f*std::min<float>(1.f,image[i][0])) << " " << (int)(255.f*std::min<float>(1.f,image[i][1])) << " " << (int)(255.f*std::min<float>(1.f,image[i][2])) << " ";
	f << std::endl;
	f.close();
    }
    
    Vec3 rayTrace( Ray const & ray , unsigned int nbRebonds = 10 ) { // rebonds ? 
	Vec3 color;
	IntersectionWithScene sceneIntersect = intersect(ray);
	color = sceneIntersect.mat.diffuseColor;
	if(sceneIntersect.intersectionExist){
	    Vec3 lightPos = Vec3( 
			(((float)rand()*light.radius)/(float)(RAND_MAX))*light.position[0],
			(((float)rand()*light.radius)/(float)(RAND_MAX))*light.position[1],
			(((float)rand()*light.radius)/(float)(RAND_MAX))*light.position[2]
			);
	    
	    //lightPos = light.position;
		    Ray raylight(sceneIntersect.p, lightPos);
		    IntersectionWithScene lightIntersect = intersect(raylight); // intersection entre cette sphere et la lumière 
		    if(lightIntersect.intersectionExist){	
			double lambda = (lightIntersect.p-raylight.origin()).length(); // distance sphere lumière
			if(lambda>= (sceneIntersect.p-raylight.origin()).length()){
			     color = sceneIntersect.mat.diffuseColor/4;
			   
			}
		    }
		    
	    return color;
	}
	return Vec3(0,0,0); // background
    }

    IntersectionWithScene intersect(Ray const & ray){
	IntersectionWithScene res;
	res.intersectionExist = false;
	
	double lambdaMin = 100000000; //big value on va regarder le lambda minimum pour savoir qulle sphere est devant // intersectée en premier
	for (unsigned int s = 0; s < spheres.size(); s++){
	    Sphere  & sphere = spheres[s];
	    IntersectionWithScene inte =  sphere.intersection(ray); // on recupère la distance cam - sphere 
	    if (inte.intersectionExist){
		double lambda = (inte.p-ray.origin()).length();
		if(lambda > 0.0 && lambda < lambdaMin){
		    lambdaMin = lambda;
		    res = inte;
		}
	    }
	}
	res.intersectionExist = lambdaMin < 100000000;
	return res;
    }
};
#endif
