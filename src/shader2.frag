
#version 120

//const float attenuationConstant = 0.8;
//const float attenuationLinear = 0.8;
//const float attenuationQuadratic = 0.8;

uniform vec3 inputLightPosition;
//uniform vec4 inputLightMaterial;
uniform bool lightIsInCamSpace;

uniform vec4 inputObjectAmbiantMaterial ;
uniform vec4 inputObjectDiffuseMaterial ;
uniform vec4 inputObjectSpecularMaterial ;
uniform float ambiantComposant;
uniform float diffuseComposant;
uniform float specularComposant;
uniform float inputObjectShininess ;

uniform bool activeLights[4];

uniform mat4 modelviewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

uniform sampler2D uTextureColor;
//uniform sampler2D uTextureNormal;

varying vec4 P; // Interpolated fragment-wise position
varying vec3 N; // Interpolated fragment-wise normal
varying vec4 C; // Interpolated fragment-wise color
varying vec4 UV;

void main (void) {
    vec4 PModelview = modelviewMatrix * P;
    vec3 position = vec3 (PModelview.xyz / PModelview.w);
   // vec3 normal = 2.0 * texture2DProj(uTextureNormal, UV).xyz - vec3(1,1,1);
    vec3 normal = normalize(normalMatrix * N);

    vec3 V = normalize(-position);

    vec3 lightPos = inputLightPosition;
    if( !lightIsInCamSpace ) {
        vec4 LModelview = modelviewMatrix * vec4(lightPos.xyz,1.0);
        lightPos = vec3 (LModelview.xyz) / LModelview.w;
    }
    vec3 L = normalize(lightPos - position);
    
    vec3 R = normalize( - L + 2.0*dot(L , normal) * normal );

    float lightAttenuation = 1.0;

    vec4 colorInTexture = texture2DProj(uTextureColor, UV); // on retrouve la couleur dans la texture
    vec4 Ia = ambiantComposant*inputObjectAmbiantMaterial;
    vec4 Id = diffuseComposant*min(1.0, max(dot(L , normal),0.0))*inputObjectDiffuseMaterial;
    vec4 Is = specularComposant*pow( max(dot(R , V) , 0.0), inputObjectShininess )*inputObjectSpecularMaterial;

    gl_FragColor = clamp(colorInTexture*(Ia + Id + Is), 0., 1.);
}
