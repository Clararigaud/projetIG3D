#include "Mesh.h"
#include "Triangle.h"
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>
void Mesh::loadOFF (const std::string & filename) {
    std::ifstream in (filename.c_str ());
    if (!in)
        exit (EXIT_FAILURE);
    std::string offString;
    unsigned int sizeV, sizeT, tmp;
    in >> offString >> sizeV >> sizeT >> tmp;
    V.resize (sizeV);
    T.resize (sizeT);
    for (unsigned int i = 0; i < sizeV; i++)
        in >> V[i].p;
    int s;
    for (unsigned int i = 0; i < sizeT; i++) {
        in >> s;
        for (unsigned int j = 0; j < 3; j++)
            in >> T[i].v[j];
    }
    in.close ();
}

void Mesh::loadOBJ (const std::string & filename) { // Will work for triangular faces only

    std::vector<Vec3 > temp_vertices;
    std::vector< Vec3 > temp_uvs;
    char materialName[128];
    int nbTriangle = 0;
    FILE * file = fopen(filename.c_str (), "r");
    if( file == NULL ){
        exit (EXIT_FAILURE);
    }
    bool hasUV = false;
    bool hasNormal = false;
    char lineHeader[128];
    while( 1 ){
        // read the first word of the line
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF)
            break; // EOF = End Of File. Quit the loop.

        if ( strcmp( lineHeader, "v" ) == 0 ){
            //std::cout << "v line "<< std::endl;
            Vec3 vertex;
            fscanf(file, "%f %f %f\n", &vertex[0], &vertex[1], &vertex[2] );
            //std::cout << "Vertex" << vertex[0]<< " "<< vertex[1]<< " "<< vertex[2]<< " "<< std::endl;
            temp_vertices.push_back(vertex);

        }else if ( strcmp( lineHeader, "vt" ) == 0 ){
            hasUV = true;
            //std::cout << "vt line "<< std::endl;
            Vec3 uv;

            fscanf(file, "%f %f\n", &uv[0], &uv[1] );
            //std::cout << "UV" << uv[0]<< " "<< uv[1]<< std::endl;
            temp_uvs.push_back(uv);
        
        // else if ( strcmp( lineHeader, "vn" ) == 0 ){
        //     glm::vec3 normal;
        //     fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z );
        //     temp_normals.push_back(normal);
         }else if ( strcmp(lineHeader, "usemtl") == 0){
            int matches = fscanf(file, "%s", materialName);
         }

         else if ( strcmp( lineHeader, "f" ) == 0 ){
            //std::cout << "f line "<< std::endl;
            //std::string v1, v2, v3;
            unsigned int vertexIndex[3], uvIndex[3];
            if(hasUV){
                int matches = fscanf(file, "%d/%d %d/%d %d/%d\n", &vertexIndex[0], &uvIndex[0],  &vertexIndex[1], &uvIndex[1], &vertexIndex[2], &uvIndex[2]);
                if (matches != 6){
                    printf("Probleme with faces\n");
                    exit(EXIT_FAILURE);
                }

            }else{
                int matches = fscanf(file, "%d %d %d\n", &vertexIndex[0], &vertexIndex[1], &vertexIndex[2]);
                if (matches != 3){
                    printf("Probleme with faces\n");
                    exit(EXIT_FAILURE);
                }
            }

            MeshTriangle triangle;
            triangle.v[0] = 0+3*nbTriangle;
            triangle.v[1] = 1+3*nbTriangle;
            triangle.v[2] = 2+3*nbTriangle;
            T.push_back(triangle);
            // std::cout << "Triangle " <<  triangle.v[0] << " " << triangle.v[1] << " " << triangle.v[2]<< std::endl;

            MeshVertex vertex1;
            vertex1.p = temp_vertices[vertexIndex[0]-1];
            MeshVertex vertex2;
            vertex2.p = temp_vertices[vertexIndex[1]-1];   
            MeshVertex vertex3;
            vertex3.p = temp_vertices[vertexIndex[2]-1];

            if(hasUV){
                vertex1.u = temp_uvs[uvIndex[0]-1][0];
                vertex1.v = temp_uvs[uvIndex[0]-1][1];
                vertex2.u = temp_uvs[uvIndex[1]-1][0];
                vertex2.v = temp_uvs[uvIndex[1]-1][1];
                vertex3.u = temp_uvs[uvIndex[2]-1][0];
                vertex3.v = temp_uvs[uvIndex[2]-1][1];
            }

            // unsigned int size = V.size()+3;
            V.resize(1+std::max(std::max(triangle.v[0], triangle.v[2] ),triangle.v[1]));

            V[triangle.v[0]] = vertex1;
            // std::cout << "V1 " <<  triangle.v[0] << " " << vertex1.p[0] << " "<<vertex1.p[1] << " "<<vertex1.p[2] << std::endl;
            V[triangle.v[1]] = vertex2;
            // std::cout << "V2 " <<  triangle.v[1] << " " << vertex2.p[0] << " "<<vertex2.p[1] << " "<<vertex2.p[2] << std::endl;
            V[triangle.v[2]] = vertex3;
            // std::cout << "V3 " <<  triangle.v[2] << " " << vertex3.p[0] << " "<<vertex3.p[1] << " "<<vertex3.p[2] << std::endl;
            nbTriangle++;
        }
    }
}

void Mesh::recomputeNormals () {
    for (unsigned int i = 0; i < V.size (); i++)
        V[i].n = Vec3 (0.0, 0.0, 0.0);
    for (unsigned int i = 0; i < T.size (); i++) {
        Vec3 e01 = V[T[i].v[1]].p -  V[T[i].v[0]].p;
        Vec3 e02 = V[T[i].v[2]].p -  V[T[i].v[0]].p;
        Vec3 n = Vec3::cross (e01, e02);
        n.normalize ();
        for (unsigned int j = 0; j < 3; j++)
            V[T[i].v[j]].n += n;
    }
    for (unsigned int i = 0; i < V.size (); i++)
        V[i].n.normalize ();
}

void Mesh::centerAndScaleToUnit () {
    Vec3 c(0,0,0);
    for  (unsigned int i = 0; i < V.size (); i++)
        c += V[i].p;
    c /= V.size ();
    float maxD = (V[0].p - c).length();
    for (unsigned int i = 0; i < V.size (); i++){
        float m = (V[i].p - c).length();
        if (m > maxD)
            maxD = m;
    }
    for  (unsigned int i = 0; i < V.size (); i++)
        V[i].p = (V[i].p - c) / maxD;
}