#ifndef Sphere_H
#define Sphere_H
#include "Vec3.h"
#include "Mesh.h"
#include "Ray.h"



class Sphere : public Mesh {
    float m_radius;
    Vec3 meshposition;
   
    public:
    unsigned int patch_h;
    unsigned int patch_v;
	Sphere() : Mesh() {
	    m_radius = 1;
	    meshposition = Vec3(0.0f,0.0f,0.0f);
	    patch_h = 50;
	    patch_v = 50;

    }

    Sphere(const Vec3 center, float radius) : Mesh(), meshposition (center), m_radius (radius)
    {
	    //meshposition = center;
	    patch_h = 50;
	    patch_v = 50;
    }

    void draw() const {
	if( trianglesArray.size() == 0 ) return;
	glEnableClientState(GL_VERTEX_ARRAY) ;
	glEnableClientState (GL_NORMAL_ARRAY);
	glNormalPointer (GL_FLOAT, 3*sizeof (float), (GLvoid*)(normalsArray.data()));
	glVertexPointer (3, GL_FLOAT, 3*sizeof (float) , (GLvoid*)(positionsArray.data()));
	glDrawElements(GL_TRIANGLES, trianglesArray.size(), GL_UNSIGNED_INT, (GLvoid*)(trianglesArray.data()));
    }

    void setSphere(unsigned int patch_h, unsigned int patch_v){
	float radius = m_radius;
	Vec3 center = meshposition;	
	int ini_size = V.size();
	for (size_t i = 0; i <= patch_v; i++) {
	    for (size_t j = 0; j <= patch_h; j++) {
	    float u = (float)(i) / (float)(patch_v),
	    v = (float)(j) / (float)(patch_h);
	    float theta = 2*M_PI * u,
	    phi   = -0.5*M_PI + v * M_PI;
	    Vec3 n(sin(theta)*cos(phi),
	    cos(theta)*cos(phi),
	    sin(phi));
	    Vec3 p = radius * n + center;
	    V.push_back(MeshVertex(p, n, u, v));
	    }
	}

	for (size_t i = 0; i < patch_v; i++) {
	    for (size_t j = 0; j < patch_h; j++) {
		T.push_back(MeshTriangle(
		    ini_size + (patch_h+1) * (i) + (j),
		    ini_size + (patch_h+1) * (i) + (j + 1),
		    ini_size + (patch_h+1) * ((i+1)) + (j + 1)
		));
		T.push_back(MeshTriangle(
		    ini_size + (patch_h+1) * (i) + (j),
		    ini_size + (patch_h+1) * ((i+1)) + (j+1),
		    ini_size + (patch_h+1) * ((i+1)) + (j)
		));
	    }
	}
    }
	
    IntersectionWithScene intersection(Ray const & ray){ // ray = direct et origine
	IntersectionWithScene res;
	res.intersectionExist = false;
	res.mat = material;
	Vec3 const & d = ray.direction();
	Vec3 pc = ray.origin() - meshposition;
	double delta = 4 * (Vec3::dot(pc, d) * Vec3::dot(pc,d)) - 4 * ((pc).squareLength() - m_radius*m_radius);      // :: appel methode de classe
	if(delta<0.0){
	    res.intersectionExist = false;
	    return res;
	}

	double lambda = -Vec3::dot(pc,d) - sqrt(delta)/2.0;
	if(lambda<0.0){ // ha c'est negatif on regarde l'autre solution
	    lambda = -Vec3::dot(pc,d) + sqrt(delta)/2.0;
	    if(lambda<0.0){ // la sphere est derrière nous 
		res.intersectionExist = false;
		return res;
	    }
	}else{
	    double lambda2 = -Vec3::dot(pc,d) + sqrt(delta)/2.0;
	    if(lambda2>=0.0 && lambda2<lambda){  
		lambda = lambda2;
	    }
	    
	}
	res.intersectionExist = true; // lambda positif on retourne la distance entre la cam et l'intersection
	res.p = ray.origin() + lambda *d;
	res.n = (res.p - meshposition);
	res.n.normalize();
	return res;
    }
};
#endif
